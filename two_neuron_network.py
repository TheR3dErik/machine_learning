import numpy as np

np.random.seed(1000)

weight = np.random.random((3,1))
#weight = np.array([[0.0],[0.0],[1.0]])
#weight = np.array([[1.74],[-0.75]])
#bias = np.array([[0]])
bias = np.random.random((1,1))

def activation(x):
    #z = np.zeros(x.shape)
    #return np.maximum.reduce([x,z])
    return x
    #return np.max([x,0])

def single_element_deriv(x):
    #if x < 0:
        #return 0
    return 1

def normalize(x):
    xmin = np.min(x)
    xmax = np.max(x)
    return (x-xmin) / (xmax-xmin)

activation_deriv = np.vectorize(single_element_deriv)

i = normalize(1.0 * np.array([ range(21) ]).T)
isquare = i ** 2
icube = i ** 3
i = np.c_[icube, isquare, i]
print(i)
#print(isquare)
#sys.exit(0)

y = normalize(1.0 * np.array([ range(21) ]).T)

learning_rate = 0.5
m=0

runs = 100000
for x in range(runs):
    z = i.dot(weight) + bias
    #print(z)
    #print(weight)
    #print
    m = activation(z)

    #print()
    C_w = 2 * i * (m-y) * activation_deriv(z)
    #print(C_w.shape)
    #print(C_w)
    C_b = 2 * (m-y) * activation_deriv(z)
    #print(np.array([C_w.mean(axis=0)]).T.shape)
    #print(np.array([C_w.mean(axis=0)]).T)

    #weight -= learning_rate * np.average(C_w)
    #bias -= learning_rate * np.average(C_b)
    weight -= learning_rate * np.array([C_w.mean(axis=0)]).T
    bias -= learning_rate * np.array([C_b.mean(axis=0)]).T

    if x % (runs/10) == 0:
        print("Loss function after "+str(x)+" training sessions is now at: " + str(np.average((m-y)**2)))

#print("Network now predicts " + str(m*(ymax-ymin) + ymin) + " for function")
print("Network now predicts " + str(m) + " for function")
print("Loss is " + str(np.average((m-y)**2)))
print("The weights are " + str(weight))
print("The biases are " + str(bias))
#print("The weights are " + str(weight * (ymax-ymin) / (imax-imin)))
#print("The biases are " + str(bias*(ymax-ymin) + ymin - imin * (ymax-ymin) * weight / (imax-imin)))
