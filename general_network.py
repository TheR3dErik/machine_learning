import numpy as np
import mnist_loader
import copy
import pickle

from sklearn.utils import shuffle

np.random.seed(1000)

def single_relu(x, deriv=False):
    if x <= 0:
        return 0
    elif deriv:
        return 1
    return x

relu = np.vectorize(single_relu)

def sigmoid(x, deriv=False):
    if not deriv:
        return 1 / (1 + np.exp(-x))
    return sigmoid(x) * (1-sigmoid(x))

def linear(x, deriv=False):
    if not deriv:
        return x
    return np.full(x.shape, 1)

class NeuralNetwork:

    def __init__(self, layers, inputs, outputs, learning_rate=0.1, activation_func=relu):
        self.learning_rate = learning_rate
        self.num_layers = len(layers)
        self.inputs = inputs
        self.outputs = outputs
        self.num_examples = inputs.shape[0]
        self.loss = []
        self.activation_func = activation_func

        self.weights = []
        self.biases = []
        self.errors = []
        self.activations = []
        self.z_values = []
        self.errors = []
        self.delta_weights = []

        for i in range(self.num_layers-1):
            self.weights.append(np.random.random((layers[i], layers[i+1])) * 2 - 1)
            self.biases.append(np.random.random((1, layers[i+1])))
            self.errors.append(np.zeros(self.biases[i].shape))
            self.activations.append(np.zeros(self.biases[i].shape))
            self.z_values.append(np.zeros(self.biases[i].shape))
            self.delta_weights.append(np.zeros(self.biases[i].shape))
            print("The shape of biases at " + str(i) + " is actually: " + str(self.biases[i].shape))

    def feedforward(self, first_layer=None):
        #if first_layer.any() == None:
        #first_layer = self.inputs
        #print("SHAPE OF FIRST LAYER: " + str(first_layer.shape))

        self.z_values[0] = first_layer.dot(self.weights[0]) + self.biases[0]
        #print("SHAPE OF BIASES: " + str(self.biases[0].shape))
        #print("SHAPE OF WEIGHTS: " + str(self.weights[0].shape))
        #print("SHAPE OF Z VALUES: " + str(self.z_values[0].shape))
        self.activations[0] = self.activation_func(self.z_values[0])

        for i in range(1, self.num_layers-1):
            self.z_values[i] = self.activations[i-1].dot(self.weights[i]) + self.biases[i]
            self.activations[i] = self.activation_func(self.z_values[i])

        return self.activations[-1]

    def backpropagate(self):
        self.loss = self.activations[-1] - self.outputs

        #Updates errors
        self.errors[-1] = self.loss * self.activation_func(self.z_values[-1], True)
        for i in range(self.num_layers-3, -1, -1):
            self.errors[i] = self.errors[i+1].dot(self.weights[i+1].T) * self.activation_func(self.z_values[i], True)

        #Updates change in weights
        self.delta_weights[0] = self.inputs.T.dot(self.errors[0]) / self.num_examples
        for i in range(1, self.num_layers-1):
            self.delta_weights[i] = self.activations[i-1].T.dot(self.errors[i]) / self.num_examples

    def gradient_descent(self):
        for i in range(self.num_layers-1):
            self.weights[i] = self.weights[i] - self.delta_weights[i] * self.learning_rate
            self.biases[i] = self.biases[i] - self.errors[i].mean(axis=0) * self.learning_rate

    def train(self, num_epochs, print_progress=True):
        original_inputs = copy.deepcopy(self.inputs)
        original_outputs = copy.deepcopy(self.outputs)
        self.num_examples = 1000
        for i in range(num_epochs):
            shuffle(original_inputs, original_outputs)

            input_split = np.split(original_inputs, 60)
            output_split = np.split(original_outputs, 60)
            for j in range(60):
                self.inputs = input_split[j]
                self.outputs = output_split[j]

                self.feedforward()
                self.backpropagate()
                self.gradient_descent()

            #if print_progress and i % int(num_epochs / 10) == 0:
            print("Run #" + str(i) + " has a loss of: " + str(0.5 * np.sum(self.loss ** 2) / 60000))
                #print("The shape of the biases is: " + str(self.biases[0].shape))
        if print_progress:
            print("Network predicts output of: " + str(self.activations[-1]))
            print("Weights of network are: " + str(self.weights[0]))
        self.inputs = original_inputs
        self.outputs = original_outputs

if __name__ == "__main__":
    """
    network_inputs = np.array([[0.0,1.0]])
    network_outputs = np.array([[0.0,0.0,1.0]])

    nn = NeuralNetwork([2,4,3], inputs=network_inputs, outputs=network_outputs, activation_func=linear, learning_rate=0.1)
    nn.train(100)
    """

    #network_inputs = np.array([[0.0,0.0], [0.0,1.0], [1.0,0.0], [1.0,1.0]])
    #network_outputs = np.array([[0.0,0.0], [0.0,1.0], [0.0,1.0], [1.0, 0.0]])
    #network_inputs = np.array([[0,0.33,0.75,1.0]]).T
    #squared = network_inputs ** 2
    #network_inputs = np.c_[squared, network_inputs]
    #print(network_inputs)
    #print
    #network_outputs = np.array([[0,0.25,0.66,1.0]]).T

    data = mnist_loader.load_data()
    print("Shape of data[0] is: " + str(data[0].shape))
    print("Shape of data[1] is: " + str(data[1].shape))

    nn = NeuralNetwork([28*28,30,10], inputs=data[1], outputs=data[0], activation_func=relu, learning_rate=0.05)
    nn.train(2)

    network_output = open("network.dat","wb")
    pickle.dump(nn, network_output)
    network_output.close()

    print("Successfully wrote file")
