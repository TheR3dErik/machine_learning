from general_network import NeuralNetwork, sigmoid, single_relu, relu
import pickle
import numpy as np
import matplotlib.image as mpimg
import sys

network_input = open("network.dat", "rb")

nn = pickle.load(network_input)
#nn.train(10)

img = mpimg.imread('./five.png').reshape(1,28*28)
#print(img.shape)

junk_data = np.zeros((1,28*28))
part1 = np.zeros((28, 14))
part2 = np.full((28, 1), 1)
part3 = np.zeros((28, 13))
junk_data = np.c_[part1, part2, part3]
junk_data = junk_data.reshape(1,28*28)
print(junk_data.shape)
x = nn.feedforward(img)
print(x.shape)
#print(x)
np.savetxt(sys.stdout, x, '%5.2f')
