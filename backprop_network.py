import numpy as np

np.random.seed(1000)

weights2 = np.random.random((2,4)) * 2 - 1
biases2 = np.random.random((1,4))
weights3 = np.random.random((4,3)) * 2 - 1
biases3 = np.random.random((1,3))

error2 = np.zeros(biases2.shape)
error3 = np.zeros(biases3.shape)

def single_relu(x, deriv=False):
    if x <= 0:
        return 0
    elif deriv:
        return 1
    return x

relu = np.vectorize(single_relu)

def sigmoid(x, deriv=False):
    if not deriv:
        return 1 / (1 + np.exp(-x))
    return np.exp(-x) / (1 + np.exp(-x)) ** 2

inputs = np.array([[0.0,1.0]])
num_examples = inputs.shape[0]
print("Shape of inputs is: " + str(inputs.shape))
outputs = np.array([[0.0,0.0,1.0]])
print("Shape of outputs is: " + str(outputs.shape))

learning_rate = 0.1

epoch_length = 100
for i in range(epoch_length):
    #feedforward
    z2 = inputs.dot(weights2) + biases2
    a2 = sigmoid(z2)

    z3 = a2.dot(weights3) + biases3
    a3 = sigmoid(z3)

    #backpropagate to calculate gradients
    loss = a3 - outputs
    error3 = (loss) * sigmoid(z3, True)
    error2 = sigmoid(z2, True) * (error3.dot(weights3.T))

    delta_weights3 = a2.T.dot(error3) / num_examples
    delta_weights2 = inputs.T.dot(error2) / num_examples

    #Update weights and biases
    weights3 = weights3 - delta_weights3 * learning_rate
    weights2 = weights2 - delta_weights2 * learning_rate

    biases3 = biases3 - error3 * learning_rate
    biases2 = biases2 - error2 * learning_rate

    if i % int(epoch_length / 10) == 0:
        print("Run #" + str(i) + " has a loss of: " + str(0.5 * np.sum(loss ** 2)))

print("Trained output:")
print(a3)
